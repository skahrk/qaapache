class qaapache i
(
 $application_title = 'QA Web Site', # Default title
 )
{
if !$application_title {
 fail("Module ${module_name} is missing a parameter 'application title'") 
}
$apachename = $::operatingsystem ? {
centos => 'httpd',
ubuntu => 'apache2',
default => 'httpd', 
}
package { $apachename: 
ensure => present,
}
filebucket { 'qabucket':
path => false, 
server => 'foreman.localdomain',
}
file { '/var/www':
ensure => directory, 
}
file { '/var/www/html':
ensure => directory, 
}
notify { 'ClientCertMessage':
withpath => true,
#message => "The certificate name is {$clientcert}",
}
notify { 'DebugMessage':
withpath => true, 
message => 'Generating index.html using erb template',
}
file { 'index.html': 
ensure => 'file', 
backup => qabucket,
path => '/var/www/html/index.html',
content => template('qaapache/index.html.erb'),
}
if $apachename == 'httpd' {
$conffile = '/etc/httpd/conf/httpd.conf'
file { '/etc/httpd/conf/httpd.conf':
ensure => file, 
owner => 'root',
group => 'root', 
source => 'puppet:///modules/qaapache/httpd.conf', 
require => Package[$apachename],
}
}else {
$conffile = '/etc/apache/apache2.conf'
 file { '/etc/apache2/apache2.conf':
 ensure => file, 
owner => 'root', 
group => 'root', 
source => 'puppet:///modules/qaapache/apache2.conf', 
require => Package[$apachename],
 }

service { $apachename:
ensure => running, 
subscribe => File[$conffile],
}
}
}

